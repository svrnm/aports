# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=libksieve
pkgver=24.08.0
pkgrel=0
pkgdesc="KDE PIM library for managing sieves"
# armhf blocked by extra-cmake-modules
# armv7, ppc64le, s390x and riscv64 blocked by qt6-qtwebengine
# loongarch64 blocked by pimcommon
arch="all !armv7 !armhf !ppc64le !s390x !riscv64 !loongarch64"
url="https://kontact.kde.org/"
license="GPL-2.0-only"
depends_dev="
	karchive-dev
	kdoctools-dev
	ki18n-dev
	kiconthemes-dev
	kidentitymanagement-dev
	kimap-dev
	kio-dev
	kmailtransport-dev
	kmime-dev
	knewstuff-dev
	kpimtextedit-dev
	kwindowsystem-dev
	libkdepim-dev
	pimcommon-dev
	qt6-qtbase-dev
	qt6-qtwebengine-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	doxygen
	extra-cmake-modules
	graphviz
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/pim/libksieve.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/libksieve-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_QCH=ON
	cmake --build build
}

check() {
	# libksieveui-findbar-findbarbasetest, sieveeditorhelphtmlwidgettest and sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidgettest require OpenGL
	xvfb-run ctest --test-dir build --output-on-failure -E "(libksieveui-findbar-findbarbase|sieveeditorhelphtmlwidget|sieveeditor-autocreatescripts-sieveeditorgraphicalmodewidget)test"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ddfb0b9e9a231e0aa0c38a3dd6ca9cc3fccc57fc73373ad01936977b23282823f0c8b0f62ee7df378d8f4f38382599e7152dc4aca4ab70bbd49f2783882bef9d  libksieve-24.08.0.tar.xz
"
