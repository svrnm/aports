# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=artikulate
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://edu.kde.org/artikulate"
pkgdesc="Improve your pronunciation by listening to native speakers"
license="(GPL-2.0-only OR GPL-3.0-only) AND GFDL-1.2-only"
depends="kirigami2"
makedepends="
	extra-cmake-modules
	karchive5-dev
	kconfig5-dev
	kcrash5-dev
	kdoctools5-dev
	ki18n5-dev
	kirigami2-dev
	knewstuff5-dev
	kxmlgui5-dev
	qt5-qtbase-dev
	qt5-qtdeclarative-dev
	qt5-qtmultimedia-dev
	qt5-qtxmlpatterns-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/education/artikulate.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/artikulate-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=MinSizeRel \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a633ecd9369941f2d1d4e78f39434c3396d141b7ba23452b0fc6cbc1718e05c276c5998ff69252a87f3a0e53eaa6b65c962d57ebeaeef77d3d602cbbafacfdc7  artikulate-24.08.0.tar.xz
"
