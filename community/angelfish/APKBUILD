# Contributor: Jonah Brüchert <jbb@kaidan.im>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=angelfish
pkgver=24.08.0
pkgrel=0
pkgdesc="Small Webbrowser for Plasma Mobile"
# armhf blocked by extra-cmake-modules
# ppc64le, s390x and riscv64 blocked by qt6-qtwebengine
# loongarch64 blocked by purpose
arch="all !ppc64le !s390x !armhf !riscv64 !loongarch64"
url="https://phabricator.kde.org/source/plasma-angelfish/"
license="GPL-3.0-or-later"
depends="
	kirigami-addons
	kirigami
	purpose
	qt6-qtbase-sqlite
	"
makedepends="
	corrosion
	extra-cmake-modules
	futuresql-dev
	kdeclarative-dev
	ki18n-dev
	kio-dev
	kirigami-addons-dev
	kirigami-dev
	libplasma-dev
	purpose-dev
	qqc2-desktop-style-dev
	qt6-qtwebengine-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/network/angelfish.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/angelfish-$pkgver.tar.xz"
options="net" # net required to download Rust dependencies

provides="plasma-angelfish=$pkgver-r$pkgrel" # Backwards compatibility
replaces="plasma-angelfish"                  # Backwards compatibility

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib \
		-DBUILD_SHARED_LIBS=True
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure -j1
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
194786940f4cc33cb5d0808ffde53332e86711807a04bf95f1800f4b700c030ee2fb50c4d4fa47181896d32960a9a4cb016b8951fc37be5bdb5401dcea6b84b7  angelfish-24.08.0.tar.xz
"
