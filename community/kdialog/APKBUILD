# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kdialog
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://www.kde.org/"
pkgdesc="A utility for displaying dialog boxes from shell scripts"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdbusaddons-dev
	kguiaddons-dev
	kiconthemes-dev
	kio-dev
	knotifications-dev
	ktextwidgets-dev
	kwindowsystem-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kdialog.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kdialog-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
ffb3fc5740ad7632405db68b430335b313a568310a103a02a87371d5b1b9d7752537fb968da9033444c929e5ea283881703b21e684d45b682d7ccd568fe1e6b7  kdialog-24.08.0.tar.xz
"
