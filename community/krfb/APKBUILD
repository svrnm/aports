# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=krfb
pkgver=24.08.0
pkgrel=0
arch="all !armhf"
url="https://kde.org/applications/internet/org.kde.krfb"
pkgdesc="Desktop sharing"
license="GPL-3.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kcompletion-dev
	kconfig-dev
	kcoreaddons-dev
	kcrash-dev
	kdbusaddons-dev
	kdnssd-dev
	kdoctools-dev
	ki18n-dev
	knotifications-dev
	kpipewire-dev
	kstatusnotifieritem-dev
	kwallet-dev
	kwayland-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	libvncserver-dev
	pipewire-dev
	plasma-wayland-protocols
	qt6-qtbase-dev
	samurai
	xcb-util-dev
	xcb-util-image-dev
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/network/krfb.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/krfb-$pkgver.tar.xz"
options="!check" # No tests

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
bc66bebbdf1ddb6c2b9155da224c5171f746e9880bd067a82fd5f570649f8346a16d2003320724ed91331ab3a35617e7a9323ae45fa59e0555b41884dd0e4113  krfb-24.08.0.tar.xz
"
