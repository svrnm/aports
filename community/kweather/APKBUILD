# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kweather
pkgver=24.08.0
pkgrel=0
pkgdesc="Weather application for Plasma Mobile"
url="https://invent.kde.org/plasma-mobile/kweather"
# armhf blocked by qt6-qtdeclarative
arch="all !armhf"
license="GPL-2.0-or-later AND CC-BY-4.0"
depends="
	kirigami-addons
	kirigami
	kquickcharts
	"
makedepends="
	extra-cmake-modules
	kconfig-dev
	kcoreaddons-dev
	ki18n-dev
	kirigami-addons-dev
	kirigami-dev
	knotifications-dev
	kquickcharts-dev
	kweathercore-dev
	libplasma-dev
	qt6-qtbase-dev
	qt6-qtcharts-dev
	qt6-qtdeclarative-dev

	qt6-qtsvg-dev
	samurai
	"
subpackages="$pkgname-lang"
_repo_url="https://invent.kde.org/utilities/kweather.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kweather-$pkgver.tar.xz"
# No tests
options="!check"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
d32525bef5a62425930b0bfd2eeeeedcf486e0f9296d551ca8afccd91abcddbd2771aa7238ee85fe0920495a91d6698bc66c6983a79c4257de77443b84d518c1  kweather-24.08.0.tar.xz
"
