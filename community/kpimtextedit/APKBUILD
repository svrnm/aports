# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kpimtextedit
pkgver=24.08.0
pkgrel=0
pkgdesc="Advanced text editor which provide advanced html feature"
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://api.kde.org/kdepim/kpimtextedit/html"
license="LGPL-2.0-or-later AND GPL-2.0-or-later"
depends_dev="
	grantlee-dev
	kcodecs-dev
	kconfig-dev
	kconfigwidgets-dev
	kcoreaddons-dev
	ki18n-dev
	kiconthemes-dev
	kio-dev
	ktextaddons-dev
	kwidgetsaddons-dev
	kxmlgui-dev
	qt6-qtbase-dev
	qt6-qtspeech-dev
	sonnet-dev
	syntax-highlighting-dev
	"
makedepends="$depends_dev
	extra-cmake-modules
	qt6-qttools-dev
	samurai
	"
checkdepends="xvfb-run"
subpackages="$pkgname-dev $pkgname-lang"
_repo_url="https://invent.kde.org/pim/kpimtextedit.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kpimtextedit-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	# kpimtextedit-plaintext-textgotolinewidgettest,
	# kpimtextedit-composerng-richtextcomposertest and
	# kpimtextedit-composerng-richtextcomposercontrolertest require OpenGL
	# kpimtextedit-texttospeech-texttospeechwidgettest requires texttospeech
	# kpimtextedit-texttospeech-texttospeechactionstest and
	# kpimtextedit-grantleebuilder-texthtmlbuildertest are broken
	local skipped_tests="kpimtextedit-("
	local tests="
		plaintext-textgotolinewidget
		texttospeech-texttospeechwidget
		texttospeech-texttospeechactions
		composerng-richtextcomposer
		composerng-richtextcomposercontroler
		grantleebuilder-texthtmlbuilder
		"
	for test in $tests; do
		skipped_tests="$skipped_tests|$test"
	done
	skipped_tests="$skipped_tests)test"
	xvfb-run ctest --test-dir build --output-on-failure -E "$skipped_tests"
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}
sha512sums="
7051d96b06806bf545ee7db8e59c004ef27f8d3da4503fb20cf250da748c25f7dceaebf39e54e1be05273c103cfe7db323ef1010da0a5c77e6da898e9d6229bf  kpimtextedit-24.08.0.tar.xz
"
