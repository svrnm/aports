# Contributor: Bart Ribbers <bribbers@disroot.org>
# Maintainer: team/kde <bribbers@disroot.org>

# The group tag is just to easily find this APKBUILD by some scripts for automation
# group=kde-applications
pkgname=kmag
pkgver=24.08.0
pkgrel=0
# armhf blocked by extra-cmake-modules
arch="all !armhf"
url="https://kde.org/applications/utilities/org.kde.kmag"
pkgdesc="A screen magnifier"
license="GPL-2.0-or-later AND GFDL-1.2-only"
makedepends="
	extra-cmake-modules
	kdoctools-dev
	ki18n-dev
	kio-dev
	kxmlgui-dev
	qt6-qtbase-dev
	samurai
	"
subpackages="$pkgname-doc $pkgname-lang"
_repo_url="https://invent.kde.org/accessibility/kmag.git"
source="https://download.kde.org/stable/release-service/$pkgver/src/kmag-$pkgver.tar.xz"

build() {
	cmake -B build -G Ninja \
		-DCMAKE_BUILD_TYPE=RelWithDebInfo \
		-DCMAKE_INSTALL_PREFIX=/usr \
		-DCMAKE_INSTALL_LIBDIR=lib
	cmake --build build
}

check() {
	ctest --test-dir build --output-on-failure
}

package() {
	DESTDIR="$pkgdir" cmake --install build
}

sha512sums="
a46c3c7cc165c708c464a1abce61c820ce5cc1e18683a7c7347ab21aff2c2bedc426bf468580a485e2ce9630ece5191dc5fa39021155cf2c163f4bca79db0292  kmag-24.08.0.tar.xz
"
